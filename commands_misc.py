import re
import string
from discord.ext import commands


class CommandsMisc(object):
  def __init__(self, bot):
    self.bot = bot

  @commands.command(name='nope', pass_context=True)
  async def nope(self, context):
    await self.bot.say("**I reject your reality and substitute my own**  //  https://gfycat.com/AmpleExcellentDevilfish")
    