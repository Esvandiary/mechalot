import sys
sys.path.append('./discord')

import discord
from discord.ext import commands
import json
import logging

from edts.edtslib import defs
from edts.edtslib import util

from commands_edts import CommandsEDTS
from commands_misc import CommandsMisc

log = logging.getLogger('mechalot')
log.setLevel(logging.INFO)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter(fmt="[%(asctime)-8s.%(msecs)03d] [%(name)-10s] [%(levelname)7s] %(message)s", datefmt=defs.log_dateformat))
log.addHandler(handler)

# Now it's set up, get the same logger wrapped in our format-modifier
log = util.get_logger('mechalot')

log.info("Reading config...")
try:
  with open('config.json', 'r') as f:
    config = json.loads(f.read())
  log.info("Config loaded")
except Exception as ex:
  log.error("Failed to read config: {}", str(ex))

bot = commands.Bot(command_prefix='.', description='Alot of Bot', pm_help=True)
bot.add_cog(CommandsEDTS(bot))
bot.add_cog(CommandsMisc(bot))
bot.run(config['auth-token'])
