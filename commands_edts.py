import re
import string
import struct
from discord.ext import commands
from edts.edtslib import env, pgnames, system, vector3

_re_hex_id64 = re.compile('^[0-9a-fA-F]{16}$')

limits = {
  'findsystems.output': 10,
  'location.close_to': 5,
  'location.distance': 500
}

class CommandsEDTS(object):
  def __init__(self, bot):
    self.bot = bot
    env.start()
  
  def __del__(self):
    env.stop()

  @commands.command(name='swapendian')
  async def swapendian(self, data : str):
    # Is this an ID64?
    if _re_hex_id64.search(data) is not None:
      s = int(data, 16)
    elif data.isdigit():
      s = int(data)
    else:
      await self.bot.say("Could not recognise valid input :(")
      return
    swapped = struct.unpack('<Q', struct.pack('>Q', s))[0]
    msg = "``` Input:  {0:016X}  /  {0}\nOutput:  {1:016X}  /  {1}```"
    await self.bot.say(msg.format(s, swapped))
  
  @commands.command(name='system', aliases=['body'])
  async def system(self, data : str):
    # Is this an ID64?
    body = 0
    if _re_hex_id64.search(data) is not None:
      id = int(data, 16)
      s = system.from_id64(id)
      body = system.mask_id64_as_body(id)
    elif data.isdigit():
      id = int(data)
      s = system.from_id64(id)
      body = system.mask_id64_as_body(id)
    else:
      s = system.from_name(data)
      # Reinterpret PG systems via ID64 to ensure we spot masquerading HA systems
      if s is not None and s.id64 is not None and isinstance(s, system.PGSystem):
        new_s = system.from_id64(s.id64)
        if isinstance(new_s, system.KnownSystem):
          s = new_s
    if s is not None:
      msg = "```    Name:  {}\n  PGName:  {}\n    ID64:  {}\nLocation:  {}\n  Sector:  {}\nPGSector:  {}{}```"
      id64str = "{}  /  {}".format(s.id64, s.pretty_id64('HEX')) if s.id64 is not None else "unknown"
      location = "[{0:.5f}, {1:.5f}, {2:.5f}]".format(s.position.x, s.position.y, s.position.z)
      bodystr = "\n  BodyID:  {}".format(body) if body != 0 else ""
      if s.uncertainty > 0.0:
        location += " ± {0:.0f}LY/axis".format(s.uncertainty)
      pgsector = "{} @ ({}, {}, {})".format(s.pg_sector.name, s.pg_sector.x, s.pg_sector.y, s.pg_sector.z)
      await self.bot.say(msg.format(s.name, s.pg_name if s.pg_name is not None else "unknown", id64str, location, s.sector.name, pgsector, bodystr))
    elif data.lower() == "raxxla":
      msg = "```    Name:  C͍̮̲͉̱̳ͦ̽̚o̤̙̺ͭr̩̦̞̓ͣ͑͊̃́̽e̠ ͋̓͏̣͖͍͓͙S̹̅̚ͅy̱̠̖͔͙̽ͅs̭͎̼̥̼̗̓ͅ ͍̩̉S͔̰̣̥ͦ̀̂͊̅e͔̝̖̼͇͎̥ͦͮͭͯ̓̅̈c̡̫͔̱̿t̜͔̆̃͝ö̹͙̭̳̜̪́r͈̭͉̝̙͓̻ͧ̄ͦͥ ͕̩ͨ̏̈́Gͯ̆ͬͩ͐̔X̊͂͐͗͑̀̚͏̝̥̳̖̟-̫̪͛́ͪͦ͞F̥̫̃͌̓̈́ ̸̬̹͎͇͖̜ͥg͉̀ͦ̂̈̊͌3̴͉͚͛ͪ͐͆8̡̠͖́͌́-̶̪͆0̖̘̜̪̪͗ͥͩ̓ͅ\n  PGName:  W̡̅̒͗r̡͖͕͚͑̚e̝̯̾ͬͪ̕g̮͔̜̈͌ͪ̿ͤo̫̮̳͆͗̓͒̌e͓͈̿̌͊̄ ͓͒̑͊̾͆̎M̠̫̒̍̿ͤ̀̀Yͥ͑ͦ͝-̠̳̼̬͔͓̹̒̎̉͌̏̏D͙̣̝̦̫̱̼̓͆͂ ̶͗̉̌̈́g̮͍͙͚͙̲̝͊3͓̤͍̥̱̤̥ͥ̋̏̀͒9̹͆ͣ-̥͌͑̏0̫̲̺̬͙̲̤ͥ͒̑ͤ͆\n    ID64:  REDACTED  /  REDACTED\nLocation:  REDACTED\n  Sector:  Ć̜͎͉o͖͢r̩̯̦̮̜̥͛ͮ͛͌̈́̇̀e͉͂́ͥͦ̎ ͍͓͡S̩̝̻̩̫̰͐y̗̖̹͒̾͗͞s̶̗̼͖̠̣͙̣̓͋ͫ̏ ̙̤̱͍̬̠́Sͬ̾e̝̤͎̹͇̯̓ͅc̛̹͔͉͊ͩ̋t̗͓o̮ͅr̵͋̓\nPGSector:  W̩̆r͖̳̬̱̜̦ͪ̋̌ͭͩe͚̥̘͓͔̖̜͢ǵ̪̹͕̤̀̿̑͢o̥ͦ̆̈̽e͒̇̐͆҉̭͕```"
      await self.bot.say(msg)
    else:
      await self.bot.say("System could not be found for query '{}'".format(data))

  @commands.command(name='sector', aliases=['region'])
  async def sector(self, data : str):
    s = pgnames.get_sector(data)
    if s is not None:
      if s.sector_class == "ha":
        msg = "```    Name:  {0}\n    Type:  Hand-Authored Region{1}\n  Centre:  {2}\n"
        msg += "  Radius:  {3:.1f}LY\nPGCentre:  {4}\n Permit?:  {5}```"
        type_ext = " (Composite)" if len(s.spheres) > 1 else ""
        location = "[{0:.5f}, {1:.5f}, {2:.5f}]".format(s.centre.x, s.centre.y, s.centre.z)
        pgsector = pgnames.get_sector(s.centre, allow_ha=False)
        pgcentre = "{} @ ({}, {}, {})".format(pgsector.name, pgsector.x, pgsector.y, pgsector.z)
        await self.bot.say(msg.format(s.name, type_ext, location, s.radius, pgcentre, "Yes" if s.needs_permit else "No"))
      else:
        msg  = "```    Name:  {0}\n    Type:  Procedurally-Generated Sector\n  Centre:  {1}\n  Origin:  {2}\n"
        msg += "    Size:  {3:.1f}LY\n SCoords:  {4}\n      ID:  {5}\n Permit?:  {6}```"
        centre = "[{0:.5f}, {1:.5f}, {2:.5f}]".format(s.centre.x, s.centre.y, s.centre.z)
        origin = "[{0:.5f}, {1:.5f}, {2:.5f}]".format(s.origin.x, s.origin.y, s.origin.z)
        scoords = "({}, {}, {})".format(s.x, s.y, s.z)
        await self.bot.say(msg.format(s.name, centre, origin, s.size, scoords, s.offset, "Yes" if s.needs_permit else "No"))
    else:
      await self.bot.say("Sector could not be found for query '{}'".format(data))

  @commands.command(name='findsystems')
  async def findsystems(self, data : str):
    if data.count('*') == 0 or (len(data) >= 3 and data.count('*') <= 1) or (len(data) >= 5 and data.count('*') <= 2):
      message = await self.bot.say("```Searching...```")
      to_output = []
      count = 0
      with env.use() as edtsenv:
        results = edtsenv.find_systems_by_glob(data)
        for result in results:
          if count < limits['findsystems.output']:
            to_output.append(result)
          count += 1
      if any(to_output):
        maxlen = max(len(s.name) for s in to_output)
        outputs = ['{0} @ [{1:.5f}, {2:.5f}, {3:.5f}]'.format(s.name.rjust(maxlen), s.position.x, s.position.y, s.position.z) for s in to_output]
        if count > limits['findsystems.output']:
          output = '```{}\n... + {} more results```'.format('\n'.join(outputs), count - limits['findsystems.output'])
        else:
          output = '```{}```'.format('\n'.join(outputs))
        await self.bot.edit_message(message, output)
      else:
        await self.bot.edit_message(message, "```No matching results found```")
    else:
      await self.bot.say("Please don't query with so many wildcards :(")

  @commands.command(name = 'location')
  async def location(self, x : float, y : float, z : float):
    pos = vector3.Vector3(x, y, z)
    locstr = "[{0:.5f}, {1:.5f}, {2:.5f}]".format(pos.x, pos.y, pos.z)
    hasector = pgnames.get_sector(pos)
    pgsector = pgnames.get_sector(pos, allow_ha=False)
    if pgsector is not None:
      namemaxlen = max([len(pgsector.name), len(hasector.name)])
      # Slightly naughty use of internal function
      pgsysids = [pgnames._get_sysid_from_relpos(pos - pgsector.get_origin(c), c, format_output=True) + '?' for c in string.ascii_lowercase[0:8]]
      hasysids = [pgnames._get_sysid_from_relpos(pos - hasector.get_origin(c), c, format_output=True) + '?' for c in string.ascii_lowercase[0:8]]
      pgsectorstr = "{} @ ({}, {}, {})".format(pgsector.name, pgsector.x, pgsector.y, pgsector.z)
      haboxelsstr = 'HABoxels:  {}  {}{}\n{}{}\n{}{}\n\n'.format(hasector.name, ' ' * (namemaxlen-len(hasector.name)), ', '.join(hasysids[0:2]) + ',', ' ' * (13+namemaxlen), ', '.join(hasysids[2:5]) + ',', ' ' * (13+namemaxlen), ', '.join(hasysids[5:8])) if hasector != pgsector else ''
      pgboxelsstr = 'PGBoxels:  {}  {}{}\n{}{}\n{}{}\n\n'.format(pgsector.name, ' ' * (namemaxlen-len(pgsector.name)), ', '.join(pgsysids[0:2]) + ',', ' ' * (13+namemaxlen), ', '.join(pgsysids[2:5]) + ',', ' ' * (13+namemaxlen), ', '.join(pgsysids[5:8]))
      msg = "```Position:  {}\n\n{}{} CloseTo:  Searching...```"
      message = await self.bot.say(msg.format(locstr, haboxelsstr, pgboxelsstr))
      with env.use() as edtsenv:
        close = list(edtsenv.find_all_systems(filters=edtsenv.parse_filter_string('close_to=?,distance<?;limit=?', pos, limits['location.distance'], limits['location.close_to'])))
      closemaxlen = max(len(s.name) for s in close) if any(close) else 0
      closestr = ['{0}  ({1:.2f}LY)'.format(s.name.ljust(closemaxlen), s.distance_to(pos)) for s in close] if any(close) else ["No systems within {}LY o_o".format(limits['location.distance'])]
      msg = "```Position:  {}\n\n{}{} CloseTo:  {}```"
      await self.bot.edit_message(message, msg.format(locstr, haboxelsstr, pgboxelsstr, '\n           '.join(closestr)))
    else:
      await self.bot.say("Couldn't find details about that location!")
